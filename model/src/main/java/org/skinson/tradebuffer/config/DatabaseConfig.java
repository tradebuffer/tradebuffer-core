package org.skinson.tradebuffer.config;

import com.mysql.cj.jdbc.MysqlDataSource;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DatabaseConfig {

    static final String BEAN_DATA_SOURCE = "bean.dataSource";


    @ComponentScan(basePackages = {"org.skinson.tradebuffer.repo.jpa"})
    static class JpaConfig {

        @Value("jpa.url")
        String jpaUrl;

        @Value("jpa.username")
        String jpaUsername;

        @Value("jpa.password")
        String jpaPassword;

        @Bean(name = BEAN_DATA_SOURCE)
        @ConditionalOnMissingBean(name = BEAN_DATA_SOURCE)
        HikariDataSource h2DataSource() {
            HikariDataSource dataSource = new HikariDataSource();
            return dataSource;

        }

        @Bean(name = BEAN_DATA_SOURCE)
        @ConditionalOnProperty("jpa.url")
        MysqlDataSource mysqlDataSource() {
            MysqlDataSource dataSource = new MysqlDataSource();
            dataSource.setURL(jpaUrl);
            dataSource.setUser(jpaUsername);
            dataSource.setPassword(jpaPassword);
            return dataSource;
        }

    }



}
