package org.skinson.tradebuffer.model.order;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Trigger {

    TriggerType triggerType;



    public enum TriggerType {
        TIME,

        PRICE,

        ORDER_CLOSED,

        ORDER_OPENED,

        ORDER_EXECUTED,

        ORDER_CANCELLED;
    }
}
