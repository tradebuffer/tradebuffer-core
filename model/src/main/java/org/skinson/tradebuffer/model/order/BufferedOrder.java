package org.skinson.tradebuffer.model.order;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;

@Getter
@Setter
public class BufferedOrder {

    @Id
    long id;

    Trigger openingTrigger;

    Trigger executeTrigger;

    Trigger cancelTrigger;

    long price;

    long amount;

    //TODO swap for other than string
    String baseCurrency;

    String swapCurrency;

    int leverage;

    RawOrderType rawType;

    //TODO swap for other than string
    String platform;


    public enum RawOrderType {
        BUY,

        SELL
    }
}
