package org.skinson.tradebuffer.repo.jpa;

import org.skinson.tradebuffer.model.order.BufferedOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BufferedOrderRepository extends JpaRepository<Long, BufferedOrder> {
}
