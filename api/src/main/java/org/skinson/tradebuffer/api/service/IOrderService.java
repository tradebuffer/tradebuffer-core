package org.skinson.tradebuffer.api.service;

import org.skinson.tradebuffer.model.order.BufferedOrder;

public interface IOrderService {

    BufferedOrder openOrder();

    BufferedOrder executeOrder();

    BufferedOrder cancelOrder();

    BufferedOrder getOrderStatus();

    BufferedOrder modifyOrder();
}
