package org.skinson.tradebuffer.api.service.impl;

import org.skinson.tradebuffer.api.service.IOrderService;
import org.skinson.tradebuffer.model.order.BufferedOrder;

public class OrderServiceImpl implements IOrderService{

    public BufferedOrder openOrder() {
        return null;
    }

    public BufferedOrder executeOrder() {
        return null;
    }

    public BufferedOrder cancelOrder() {
        return null;
    }

    public BufferedOrder getOrderStatus() {
        return null;
    }

    public BufferedOrder modifyOrder() {
        return null;
    }
}
